#include "core/main.h"

#include "core/core.hpp"

/* CAN RELATED FLAGS AND DATA */
uint8_t canErrFlag = 0;
uint32_t canErrCode = 0;
uint8_t rxCount = 0;
bool canOnlineFlag = false;
uint32_t canDisconnectTime = 0;

/** TALOS SLEEP RELATED DATA */
micro_second sleepStartTime = 0;

CAN_HandleTypeDef hcan1;
Hermes_t hermes;
Talos_t talos;
Core::SelfCheck *coreSelfCheck;
Core::RepeatingTimer *repeatingTimer;

/**
 * Private function to handle CAN reconnect with Talos.
 */
void reconnect(Talos_t *talos);

void setup() {
    HAL_Init();

    Serial.begin(115200);

#ifdef CORE_LOG_SERIAL
    Serial.println("CoreSTM Startup...");
#endif

    Hermes_init(&hermes, DEVICE_ID, 8);
    Talos_init(&talos);

    canDisconnectTime = micros();

    if (HermesCAN_setup(&hermes, &hcan1) == HERMES_CAN_ERROR) {
        Core_forceError("CAN setup failed.");
        return;
    }

    if (HermesCAN_setupIt(&hcan1) == HERMES_CAN_ERROR) {
        Core_forceError("CAN IT setup failed.");
        return;
    }

    if (DEVICE_ID == 0) {
        Core_log(
            "WARNING: You are using the default DEVICE_ID, please change "
            "it in the platformio.ini file.");
    }

#ifndef CORE_STARTUP_DELAY_MS
#define CORE_STARTUP_DELAY_MS 500
#endif

    delay(CORE_STARTUP_DELAY_MS);

    coreSelfCheck = new Core::SelfCheck(&hermes);
    coreSelfCheck->enable();

    repeatingTimer = new Core::RepeatingTimer();

    /**
     * Setup selfcheck
     */
    setjmp(coreSelfCheck->jmpLoc);
    if (coreSelfCheck->requestEscape) {
        Core_forceError("Freezed in user init.");
        return;
    }

#ifdef CORE_LOG_SERIAL
    Serial.println("CoreSTM is ready.");
#endif

    coreSelfCheck->reset();
    Talos_exec(&talos);  // Execute init user code here.
    coreSelfCheck->processCheck();

    Talos_receiveSignal(&talos, initOK);
    Talos_exec(&talos);  // Does not run user code. Only used go from init to
                         // disconnected. Otherwise we go straight into error.
                         // See Talos states.
}

void loop() {
    /**
     * CAN ERROR HANDLER
     */
    canErrCode = HAL_CAN_ERROR_NONE;
    if (canErrFlag) {
        canErrCode = HAL_CAN_GetError(&hcan1);
        HAL_CAN_ResetError(&hcan1);
        canErrFlag = 0;

        Core_log("Reported CAN error: " + std::to_string(canErrCode));
        Talos_receiveSignal(&talos, CANDisconnected);
    }

    /**
     * INFINITE LOOP ESCAPE HANDLER
     */
    setjmp(coreSelfCheck->jmpLoc);
    if (coreSelfCheck->requestEscape) {
        Core_forceError("Freezed in user loop.");
        return;
    }

    /**
     * PROCESS CAN TX
     */
    if ((Hermes_waitingForTX(&hermes) &&
         (HermesCAN_processTX(&hermes, &hcan1) == HERMES_CAN_OK))) {
        reconnect(&talos);
    }

    /**
     * PROCESS CAN RX
     */
    rxCount = Hermes_getCompleteBufferCount(&hermes);
    while (rxCount != 0) {
        Hermes_triggerNextMessage(&hermes);
        rxCount--;
    }

    coreSelfCheck->reset();
    Core_priorityLoop();  // Exec priority loop
    coreSelfCheck->processCheck();
    // Make sure we are not stuck in an infinite loop.

    // Only execute user loop if minimum required period is met.
    if (repeatingTimer->is_time_up()) {
        coreSelfCheck->reset();
        Talos_exec(&talos);             // Execute init user code here.
        coreSelfCheck->processCheck();  // Make sure we are not stuck in an
                                        // infinite loop.
    }

    if (talos.currentState == INIT) {
        // only happens after softreset command. Same as after init()
        canDisconnectTime = micros();
        Talos_receiveSignal(&talos, initOK);
        Talos_exec(&talos);
    }
}

void HAL_CAN_TxMailbox0CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        reconnect(&talos);
    }
}

void HAL_CAN_TxMailbox1CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        reconnect(&talos);
    }
}

void HAL_CAN_TxMailbox2CompleteCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processTX(&hermes, hcan) == HERMES_CAN_OK) {
        reconnect(&talos);
    }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef *hcan) { canErrFlag = 1; }

void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processRX(&hermes, hcan, CAN_RX_FIFO0) == HERMES_CAN_OK) {
        reconnect(&talos);
    }
}

void HAL_CAN_RxFifo1MsgPendingCallback(CAN_HandleTypeDef *hcan) {
    coreSelfCheck->canCheck();
    if (HermesCAN_processRX(&hermes, hcan, CAN_RX_FIFO1) == HERMES_CAN_OK) {
        reconnect(&talos);
    }
}

void Hermes_onReservedMessage(Hermes_t *hermes, HermesBuffer *buffer) {
    switch (buffer->command) {
        case CORE_CMD_TALOS_RESET:
            Core_log("Remote reset requested by " + std::to_string(MOTHER_ID));
#ifndef CORE_REMOTE_HARD_RESET
            Core_softReset();
#else
            Core_hardReset();
#endif
            break;

        case CORE_CMD_TALOS_STATUS:
            Hermes_send(hermes, buffer->remote, buffer->command, true, "o",
                        talos.currentState);
            break;

        default:
            break;
    }
}

/** CORE RECONNECT BEHAVIOUR **/

void Talos_onDisconnected() {
    if (canOnlineFlag) {
    // From online to offline
#ifdef CORE_LOG_SERIAL
        Serial.println("CAN disconnected.");
#endif
        canDisconnectTime = micros();
        canOnlineFlag = false;
    } else {
        // Already offline
        if (micros() - canDisconnectTime > 1000 * CORE_CAN_TIMEOUT) {
#ifdef CORE_LOG_SERIAL
            Serial.println("CAN timed out.");
#endif
            Talos_receiveSignal(&talos, error);
        }
    }
}

void reconnect(Talos_t *ta) {
    if (!canOnlineFlag) {
        if (ta->currentState == DISCONNECTED) {
            Talos_receiveSignal(ta, CANConnected);
            Hermes_send(&hermes, MOTHER_ID, HERMES_CMD_STATUS, 0, "i",
                        talos.currentState);
#ifdef CORE_LOG_SERIAL
            Serial.println("CAN online.");
#endif
        }
        canOnlineFlag = true;
    }
}

/**Core generic functions */

void Core_softResetIfConnected() {
    if (canOnlineFlag) {
        Core_softReset();
    }
}

void Core_log(std::string message) {
    // Disabled due to ub when called twice at once
    // Hermes_log(&hermes, MOTHER_ID, (char *)message.c_str());
#ifdef CORE_LOG_SERIAL
    Serial.println((char *)message.c_str());
#endif
}

void Core_hardReset() {
    delay(100);
    HAL_NVIC_SystemReset();
}

void Core_softReset() {
    coreSelfCheck->reset();
    Talos_receiveSignal(&talos, reset);
}

void Core_forceError(std::string message) {
    if (message != "") {
        Core_log("ERROR : " + message);
    }
    Hermes_send(&hermes, MOTHER_ID, HERMES_CMD_STATUS, 0, "i", ERRORED);
    coreSelfCheck->reset();
    Talos_receiveSignal(&talos, error);
    Talos_exec(&talos);
}

void Core_forceError() { Core_forceError(""); }

void Core_setMaximumLoopFrequency(hertz frequency) {
    repeatingTimer->disable();
    repeatingTimer->setFrequency(frequency);
    repeatingTimer->enable();
}

void Core_setMinimumLoopPeriod(micro_second us) {
    repeatingTimer->disable();
    repeatingTimer->setPeriod(us);
    repeatingTimer->enable();
}

void Core_sleep(milli_second duration) {
    // meh, weird behaviour, to be reworked
    if (talos.currentState == SLEEPING || talos.currentState == SLEEPING_EVER) {
        if ((micros() - sleepStartTime) * 1000 < duration) return;
        Talos_receiveSignal(&talos, sleepEnd);
    } else if (talos.currentState == RUNNING || talos.currentState == ERRORED) {
        Talos_receiveSignal(&talos, sleep);
        sleepStartTime = micros();
    }
}