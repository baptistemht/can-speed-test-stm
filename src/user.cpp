#include "core/core.hpp"

typedef enum {IDLE, TX, RX, RT} DeviceState;

DeviceState state;
uint32_t data;
uint32_t count;
uint32_t target;
uint32_t startTime;
uint32_t received;
bool started;
bool end;
HermesReturnCode code;

HermesBuffer *buffer;
uint32_t bufferIndex;

void Talos_initialisation(){
    Serial.println("Core STM Speed test.");
    state = IDLE;
}

void Talos_loop(){
    switch (state) {
    case IDLE:
        count = 0;
        target = 10000;
        startTime = getCurrentMillis();
        started = false;
        received = false;
        end=false;
        
        Hermes_getEmptyOutputBuffer(&hermes, &buffer);
        Hermes_clearBuffer(buffer);
        Hermes_setBufferDestination(buffer, DEVICE_ID, 0x2, 0);
        bufferIndex = Hermes_getOutputBufferIndex(&hermes, buffer);

        if(Serial.available()){
            char c = Serial.read();

            switch (c){
            case 'R':
                state = RX;
                Serial.println("RX State.");
                break;
            case 'T':
                state = TX;
                Serial.println("TX State.");
                break;
            case 'X':
                state = RT;
                Serial.println("RT State.");
                break;
            default:
                state = IDLE;
                Serial.println("IDLE State.");
                break;
            }
        }
        delay(200);
        break;
    case TX:
        if(count!=target){
            code = Hermes_sendBuffer(&hermes, bufferIndex);
            if(code != HERMES_OK){
                Serial.println(code);
            };
            delayMicroseconds(64);
            /*
            below 25us, it's ludicrous.
            25us: 7.8s, 100k. 128 buffer. 128bits/buffer. 56% ram.
            15us: 7.8s, 100k. 256 buffer. 64bits/buffer.
            5us:  7.8s, 100k. 256 buffer. 64bits/buffer.
            0us:    9s, 100k, 384 buffer. 64bits/buffer. 98,6% ram.
            */
            count++;
        }else{
            Hermes_send(&hermes, DEVICE_ID, 0x1, 0, "");
            Serial.println("TX done.");
            state = IDLE;
        }
        break;
    case RX:
        if(!started){
            if(received){
                Serial.println("Go!");
                started = true;
                startTime = getCurrentMillis();
                received = false;
                count++;
            }
        }else{
            if(received!=0){
                count+=received;
                received=0;
            }
            if((getCurrentMillis() - startTime > 15000) || end){
                Serial.print("Received ");
                Serial.print(count);
                Serial.print(" messages in ");
                Serial.print(getCurrentMillis() - startTime);
                Serial.println("ms");
                Serial.print("Target: ");
                Serial.println(target);
                Serial.println("RX done");
                state = IDLE;
            }
        }
        break;
    case RT:
        break;
    default:
        Serial.println("Forbidden state.");
        delay(500);
        Core_softReset();
        break;
    }
}


void Talos_onError(){
    Serial.println("An error occured.");
    delay(500);
    Core_softReset();
}

void Core_priorityLoop(){
    // empty
}

void Hermes_onMessage (Hermes_t *hermesInstance, uint8_t sender, uint16_t command, uint32_t isResponse){
    /* USER CODE */

    if(command == 4095) return;

    switch (state){
    case RX:
        if(command==0x1) end=true;
        received++;
        HermesBuffer *buf;
        if(Hermes_getInputBuffer(hermesInstance, sender, command, &buf) == HERMES_OK){
            Hermes_rewind(buf);
            Hermes_get(buf, &data);
            //empty buffer
        }
        break;
    default:
        break;
    }
    
    
}